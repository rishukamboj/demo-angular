'use strict';

app.service('IntelligenceProductsService', function () {
            this.GetRecord = function () { return [
			{
			    "id": 1,
			    "label": "Shirt #1",
			    "data": [
                    [gd(2015, 0, 1), 2,{"Online":2,"Festival":0,"Locations":"Australia","Total":250.00}], 
                    [gd(2015, 1, 1), 4,{"Online":2,"Festival":2,"Locations":"Australia","Total":350.00}], 
                    [gd(2015, 2, 1), 5,{"Online":1,"Festival":4,"Locations":"Australia","Total":650.00}], 
                    [gd(2015, 3, 1), 2,{"Online":1,"Festival":1,"Locations":"Australia","Total":250.00}], 
                    [gd(2015, 4, 1), 4,{"Online":2,"Festival":2,"Locations":"Australia","Total":250.00}], 
                    [gd(2015, 5, 1), 3,{"Online":2,"Festival":1,"Locations":"Australia","Total":1250.00}], 
                    [gd(2015, 6, 1), 5],
	                [gd(2015, 7, 1), 6,{"Online":2,"Festival":4,"Locations":"Australia","Total":900.00}], 
                    [gd(2015, 8, 8), 10,{"Online":8,"Festival":2,"Locations":"Australia","Total":250.00}], 
                    [gd(2015, 9, 1), 3,{"Online":2,"Festival":1,"Locations":"Australia","Total":250.00}], 
                    [gd(2015, 10, 1), 35,{"Online":20,"Festival":15,"Locations":"Australia","Total":2250.00}],
                    [gd(2015, 11, 1), 2,{"Online":0,"Festival":2,"Locations":"Australia","Total":250.00}]],
			    "color": '#46bb00',
				
			}
			,
			{
			    "id": 2,
			    "label": "Shirt #2",
			    "data": [
                [gd(2015, 0, 1), 12,{"Online":2,"Festival":10,"Locations":"Australia","Total":540.00}], [gd(2015, 1, 1), 9,{"Online":5,"Festival":4,"Locations":"Australia","Total":960.00}], [gd(2015, 2, 1), 7,{"Online":2,"Festival":0,"Locations":"Australia","Total":250.00}], [gd(2015, 3, 1), 9,{"Online":2,"Festival":7,"Locations":"Australia","Total":960.00}], [gd(2015, 4, 1), 6,{"Online":5,"Festival":1,"Locations":"Australia","Total":960.00}], [gd(2015, 5, 1), 8,{"Online":5,"Festival":3,"Locations":"Australia","Total":960.00}], [gd(2015, 6, 1), 4,{"Online":0,"Festival":4,"Locations":"Australia","Total":960.00}],
					[gd(2015, 7, 1), 9,{"Online":4,"Festival":5,"Locations":"Australia","Total":1930.00}], [gd(2015, 8, 8), 5,{"Online":1,"Festival":4,"Locations":"Australia","Total":830.00}], [gd(2015, 9, 1), 2,{"Online":1,"Festival":1,"Locations":"Australia","Total":350.00}], [gd(2015, 10, 1), 40,{"Online":25,"Festival":15,"Locations":"Australia","Total":1760.00}], [gd(2015, 11, 1), 12,{"Online":8,"Festival":4,"Locations":"Australia","Total":960.00}]],
			    "color": '#fe3365'
			},
               			{
               		    "id": 3,
              			    "label": "Shirt #3",
               			    "data": [[gd(2015, 0, 1), 22,{"Online":12,"Festival":10,"Locations":"Australia","Total":9460.00}], [gd(2015, 1, 1), 19,{"Online":15,"Festival":4,"Locations":"Australia","Total":1060.00}], [gd(2015, 2, 1), 10,{"Online":6,"Festival":4,"Locations":"Australia","Total":1230.00}], [gd(2015, 3, 1), 21,{"Online":14,"Festival":7,"Locations":"Australia","Total":2840.00}], [gd(2015, 4, 1), 16,{"Online":12,"Festival":4,"Locations":"Australia","Total":1760.00}], [gd(2015, 5, 1), 18,{"Online":12,"Festival":6,"Locations":"Australia","Total":2530.00}], [gd(2015, 6, 1), 14,{"Online":11,"Festival":3,"Locations":"Australia","Total":1352.00}],
               					[gd(2015, 7, 1), 26,{"Online":16,"Festival":6,"Locations":"Australia","Total":3324.00}], [gd(2015, 8, 8), 28,{"Online":5,"Festival":23,"Locations":"Australia","Total":3432.00}], [gd(2015, 9, 1), 12,{"Online":3,"Festival":9,"Locations":"Australia","Total":1540.00}], [gd(2015, 10, 1), 36,{"Online":25,"Festival":11,"Locations":"Australia","Total":4327.00}], [gd(2015, 11, 1), 22,{"Online":12,"Festival":10,"Locations":"Australia","Total":2823.00}]],
               			    "color": '#c9f8ab'
              		}
            ];
			};
			
		    function gd(year, month, day) 
            {
                // alert(new Date(year, month, day).getTime());
                return new Date(year, month, day).getTime();
            }
				
				this.InitGraph=function()
				{
				};
            });
			 
			 
  // Flot Chart controller 
 app.controller('IntelligenceProductsCtrl', ['$scope','IntelligenceProductsService', function($scope,IntelligenceProductsService) { 

	$scope.TotalRecord= IntelligenceProductsService.GetRecord();
	$scope.dayActive = 'active';
    $scope.weekActive = '';
    $scope.monthActive = '';
	
	$scope.SelectedData=[];
		
	$scope.XxSetting={
                    /*min: (new Date(2015, 1, 1)).getTime(),
                    max: (new Date(2015, 12, 2)).getTime(),*/
                    mode: "time",
                    tickSize: [1, "month"],
                    monthNames: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                    // tickLength: 0,
                    axisLabel: 'Month',
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 11,
                    axisLabelPadding: 5,
					 color: '#ccc' 


                };
	$scope.productId=0;
	
	$scope.SetGraph = function () {
               
				var _result=false;
				if($scope.SelectedData.length>0)
				{            
					$scope.SelectedData.forEach(function(itm)
					{
					if(itm.id==$scope.productId)
					{
					_result=true;
					}
					});
				}
				if(!_result)
				{
                    $scope.TotalRecord.forEach(function (d) {
                        if (d.id == $scope.productId) {
                            $scope.SelectedData.push({
                                "id": d.id,
                                "label": d.label,
                                "data": d.data,
                                "color": d.color
                            });
                        }
                    });
				}
				
				$scope.productId=0;
				    $scope.SelectedData.length>0?InitGraph($scope.SelectedData,true):InitGraph($scope.TotalRecord,false);
            };

	$scope.SetDay=function()
	{
        $scope.dayActive = 'active'; 
        $scope.weekActive = ''; 
        $scope.monthActive = ''; 
    	$scope.XxSetting={
                /*min: (new Date(2015, 1, 1)).getTime(),
                max: (new Date(2015, 12, 2)).getTime(),*/
                mode: "time",
    			minTickSize: [1, "day"],
    			min: (new Date(2015, 1, 1)).getTime(),
    			max: (new Date(2015, 1, 7)).getTime(),
    			 color: '#ccc' ,
    			timeformat: "%a"


            };
	   $scope.SelectedData.length>0?InitGraph($scope.SelectedData,true):InitGraph($scope.TotalRecord,false);
	};

	$scope.SetWeek=function()
	{ 
        $scope.dayActive = ''; 
        $scope.weekActive = 'active'; 
        $scope.monthActive = ''; 
    	$scope.XxSetting={
                /*min: (new Date(2015, 1, 1)).getTime(),
                max: (new Date(2015, 12, 2)).getTime(),*/
                mode: "time",
    			minTickSize: [1, "day"],
    			min: (new Date(2015, 1, 1)).getTime(),
    			max: (new Date(2015, 1, 7)).getTime(),
    			 color: '#ccc' ,
    			timeformat: "%d/%m/%Y"


            };
       $scope.SelectedData.length>0?InitGraph($scope.SelectedData,true):InitGraph($scope.TotalRecord,false); 
	};

	$scope.SetMonth=function()
	{
        $scope.dayActive = ''; 
        $scope.weekActive = ''; 
        $scope.monthActive = 'active'; 
    	$scope.XxSetting={
                /*min: (new Date(2015, 1, 1)).getTime(),
                max: (new Date(2015, 12, 2)).getTime(),*/
                mode: "time",
                tickSize: [1, "month"],
                monthNames: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                // tickLength: 0,
                axisLabel: 'Month',
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 11,
                axisLabelPadding: 5,
    			 color: '#ccc' 
            };
	  $scope.SelectedData.length>0?InitGraph($scope.SelectedData,true):InitGraph($scope.TotalRecord,false); 
	};
			
	$scope.close = function (id) {
          $scope.SelectedData.splice(id, 1);
       
	  $scope.SelectedData.length>0?InitGraph($scope.SelectedData,true):InitGraph($scope.TotalRecord,false);

    };
	
    var InitGraph=  function(data,IsChart) {
            //var data1 = d;
            $.plot($("#filled_green"), data, {
                xaxis: $scope.XxSetting ,
                yaxis: {
                    axisLabel: 'Quantity',
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 11,
                    autoscaleMargin: 0.01,
                    axisLabelPadding: 5,
					 color: '#ccc' 
                    // tickLength: "full"
                },
                series: {
                    lines: {
                        show: IsChart,
                        fill: false,
                        fillColor: { colors: [{ opacity: 0.2 }, { opacity: 0.2}] },
                        lineWidth: 1.5
                    },
                    points: {
                        show: IsChart,
                        radius: 2.5,
                        fill: true,
                        fillColor: "#ffffff",
                        symbol: "circle",
                        lineWidth: 1.1
                    }
                },
                grid: { hoverable: true, clickable: true, borderWidth: 1, color: '#ccc' },
                legend: {
                    show: false
                },
                tooltip: true,
                tooltipOpts: { content: '%x.1 is %y.4',  defaultTheme: false, shifts: { x: 0, y: 20 }}
            });
		    $("#filled_green").useTooltip();
        };
		 var previousPoint = null, previousLabel = null;
		 $.fn.useTooltip = function () {
            $(this).bind("plothover", function (event, pos, item) {
                if (item) {
                    if ((previousLabel != item.series.label) || (previousPoint != item.dataIndex)) {
					console.log(item);
                        previousPoint = item.dataIndex;
                        previousLabel = item.series.label;
                        $("#tooltip").remove();
						var _sellRecord=null;
						angular.forEach(item.series.data,function(itm)
						{
							if(itm[0]==item.datapoint[0] && itm[1]==item.datapoint[1])
							{
								_sellRecord=itm[2];
							}
						});
                       
                        var totalSold = item.datapoint[1];
 
                        var color = item.series.color;
                       
 
                        //console.log(item);
 
                        if (item.seriesIndex == 0) {
                            showTooltip(item.pageX,
                            item.pageY,
                            color,                                                                          																																	//{"Online":2,"Festival":0,"Locations":"Australia","Total":250.00}
                            "<span style='border-bottom:2px solid #cccccc;width:100%;textAlign:center;'><strong>" + item.series.label + "</strong></span><br>Units Sold : <strong>" + totalSold + "</strong><br>Location : <strong>" + _sellRecord.Locations + "</strong><br>Online : <strong>" + _sellRecord.Online + "</strong><br>Festival : <strong>" + _sellRecord.Festival + "</strong><br>Total : <strong>AU $" + _sellRecord.Total + "</strong>");
                        } else {
                            showTooltip(item.pageX,
                            item.pageY,
                            color,
                            "<span style='border-bottom:2px solid #cccccc;textAlign:center;'><strong>" + item.series.label + "</strong></span><br>Units Sold : <strong>" + totalSold + "</strong><br>Location : <strong>" + _sellRecord.Locations + "</strong><br>Online : <strong>" + _sellRecord.Online + "</strong><br>Festival : <strong>" + _sellRecord.Festival + "</strong><br>Total : <strong>AU $" + _sellRecord.Total + "</strong>");
                        }
                    }
                } else {
                    $("#tooltip").remove();
                    previousPoint = null;
                }
            });
        };
 
        function showTooltip(x, y, color, contents) {
            $('<div id="tooltip">' + contents + '</div>').css({
                position: 'absolute',
                display: 'none',
                top: y-30 ,
                left: x-140,
                border: '2px solid ' + color,
                padding: '3px',
                'font-size': '11px',
                'border-radius': '5px',
                'background-color': '#fff',
                'font-family': 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
                opacity: 0.9
            }).appendTo("body").fadeIn(200);
        } 

        
    $(document).ready(function()   
        {  
            InitGraph($scope.TotalRecord,false);
        }
    );
  }]);