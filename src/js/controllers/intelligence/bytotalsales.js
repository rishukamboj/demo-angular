'use strict';

app.service('IntelligencePeriodService', function () {

	this.getLastThirty = function() 
	{
	 	var data = [];

	    for (var i = 1; i < 31; i += 1){        
	        data.push([gd(2015, 0, i), i*4]);
	    }
		var t =  
		[ 
			{
				id : 1,
	    		data: data,
	    		points: {show:true, symbol: 'circle'}, 
	    		lines: { show: true, fill: true, fillColor: { colors: [{ opacity: 0.1 }, { opacity: 0.1}]}}
			}
		]; 
	    return t;
	};

	this.getLastWeek = function() 
	{
	 	var data = [];

	    for (var i = 1; i < 8; i += 1){        
	        data.push([gd(2015, 0, i), i*4]);
	    }
		var t =  
		[ 
			{
				id : 1,
	    		data: data,
	    		points: {symbol: 'circle'}, 
	    		lines: { show: true, fill: true, fillColor: { colors: [{ opacity: 0.1 }, { opacity: 0.1}]}}
			}
		]; 
	    return t;
	};
 
	
	function gd(year, month, day) {
	    // alert(new Date(year, month, day).getTime());
	    return new Date(year, month, day).getTime();
	} 

});
			 
			 
// Flot Chart controller 
app.controller('IntelligencePeriodCtrl', ['$scope','IntelligencePeriodService', function($scope,IntelligencePeriodService) 
{ 
    $scope.chartTitle = 'Sales from 1st of January to 30th of January';
    $scope.lastWeekActive = '';
    $scope.lastThirtyActive = 'active';
	 
  
	$scope.dailySettings = 
	{ 
		colors: [ '{{app.color.info}}','{{app.color.warning}}' ],
		yaxis: {
			tickFormatter: function(val, axis){
			   var val = "$" + val.toFixed(2); 
			   return val;
			}
		},
        xaxis: { 
        	mode: "time", 
            tickSize: [1, "day"],
			color: '#ddd',
			tickFormatter: function(val, axis) {
			   var val = moment(val,'x').format("D MMM"); 
			   return val;
			}
		},
		grid: { hoverable: true, clickable: true, borderWidth: 0, color: '#ccc' },
        axisLabelUseCanvas: true,
        axisLabelFontSizePixels: 11,
        axisLabelPadding: 5,
        series: {
        	shadowSize: 3,
            lines: {
                show: true,
                fill: false,
                fillColor: { colors: [{ opacity: 0.2 }, { opacity: 0.2}] },
                lineWidth: 2
            },
            points: {
                show: true,
                radius: 2.5,
                fill: true,
                fillColor: "#ffffff",
                symbol: "circle",
                lineWidth: 1.1
            }
        },
    };

	$scope.productId=0; 


    var showGraph=  function(data,IsChart) 
    {
        //var data1 = d;
        $.plot($("#chart-period"), data, $scope.dailySettings);
		$("#chart-period").useTooltip();
    }
	 
	$scope.thirtyDaysFilter=function()
	{ 
		$scope.chartTitle = 'Sales from 1st of January to 30th of January'
	    $scope.lastWeekActive = '';
	    $scope.lastThirtyActive = 'active';
		showGraph(IntelligencePeriodService.getLastThirty(),false);
	}


	$scope.lastWeekFilter=function()
	{ 
		$scope.chartTitle = 'Sales from 23rd of January to 30th of January'
	    $scope.lastWeekActive = 'active';
	    $scope.lastThirtyActive = '';
		showGraph(IntelligencePeriodService.getLastWeek(),false);
	}


	 var previousPoint = null, previousLabel = null;
	 $.fn.useTooltip = function () {
        $(this).bind("plothover", function (event, pos, item) {
            if (item) { 
                var totalSold = item.datapoint[1]; 
                var date = item.datapoint[0];  
                var color = item.series.color;  
                showTooltip(item.pageX, item.pageY, "#FF0000", totalSold, date); 
            } else {
                shown = false;
                $("#tooltip").remove(); 
            }
        });
    };
    var shown = false;
    function showTooltip(x, y, color, soldAmount, date) { 
    	var cssOptions = {
            position: 'absolute',
            display: 'none',
            top: y ,
            left: x,
            border: '2px solid ' + color,
            padding: '3px',
            'font-size': '11px',
            'border-radius': '5px',
            'background-color': '#fff',
            'font-family': 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
            opacity: 0.9
        };
        date = moment(date, "x").format('D MMM');
    	if (shown)
    	{
        	$('<div id="tooltip">' +
				'<p>'+ date + '</p>' +
        		'<p>Total sales: AU$' + soldAmount.toFixed(2) + '</p>' +
    			'</div>').css(cssOptions);
    	}
    	else 
    	{
        	$('<div id="tooltip">' +
				'<p>'+ date + '</p>' +
        		'<p>Total sales: AU$' + soldAmount.toFixed(2) + '</p>' +
    			'</div>').css(cssOptions).appendTo("body").fadeIn(200); 
        	shown = true;
    	}
    } 


	$(document).ready(function() {showGraph(IntelligencePeriodService.getLastThirty(),false);});
}]);