'use strict';

app.service('IntelligenceBestsellersService', function () {

	this.getLastThirty = function() 
	{
	 	var data = [];

	    for (var i = 1; i < 31; i += 1){        
	        data.push([gd(2015, 0, i), i*4]);
	    }
		var t =  
		[ 
			{
				id : 1,
	    		data: data,
	    		points: {show:true, symbol: 'circle'}, 
	    		lines: { show: true, fill: true, fillColor: { colors: [{ opacity: 0.1 }, { opacity: 0.1}]}}
			}
		]; 
	    return t;
	};

	this.getLastWeek = function() 
	{
	 	var data = [];

	    for (var i = 1; i < 8; i += 1){        
	        data.push([gd(2015, 0, i), i*4]);
	    }
		var t =  
		[ 
			{
				id : 1,
	    		data: data,
	    		points: {symbol: 'circle'}, 
	    		lines: { show: true, fill: true, fillColor: { colors: [{ opacity: 0.1 }, { opacity: 0.1}]}}
			}
		]; 
	    return t;
	};
 
	
	function gd(year, month, day) {
	    // alert(new Date(year, month, day).getTime());
	    return new Date(year, month, day).getTime();
	} 

});
			 
			 
// Flot Chart controller 
app.controller('IntelligenceBestsellersCtrl', ['$scope','IntelligenceBestsellersService', function($scope,IntelligencePeriodService) 
{  
	$scope.tableTitle = 'Bestsellers from 1st of January to 30th of January'; 

	function generateRandomItem(id) {
 		
		var product = {
			sku: "SKU-" + id,
			productName: "Product#" + id,
			quantitySold: Math.floor(Math.random() * 100),
			totalSales: Math.floor(Math.random() * 2000), 
		}
		console.log(product);
		return product;
	}

	$scope.rowCollection = [];

	for (var id = 0; id < 5; id++) {
		$scope.rowCollection.push(generateRandomItem(id));
	}
	$scope.displayedCollection = [].concat($scope.rowCollection);


	$scope.applyFilters = function() {
		alert('filtering!');
		console.log($scope.filters);
	}
}]);