angular.module('xeditable').factory('XEditableExtended', function(editableController){
  BasicService['method_four'] = function(){};
  return BasicService;
});

app.controller('SalesOrdersCtrl', ['$scope', '$rootScope', '$filter', '$http', 'editableOptions', 'editableThemes', 
  function($scope,$rootScope, $filter, $http, editableOptions, editableThemes){
    editableThemes.bs3.inputClass = 'input-sm';
    editableThemes.bs3.buttonsClass = 'btn-sm';
    editableOptions.theme = 'bs3'; 

    $scope.products = [
      {'id' : 1, 'name' : 'Flyweight Hoodie #1', 'unitprice': 50.00, 'instock' : 15},
      {'id' : 2, 'name' : 'Flyweight Hoodie #2', 'unitprice': 35.00, 'instock' : 25},
      {'id' : 3, 'name' : 'Flyweight Hoodie #3', 'unitprice': 15.00, 'instock' : 35},
      {'id' : 4, 'name' : 'Flyweight Hoodie #4', 'unitprice': 10.00, 'instock' : 45},
      {'id' : 5, 'name' : 'Flyweight Hoodie #5', 'unitprice': 20.00, 'instock' : 30}
    ];

    $scope.html5 = {
      email: 'email@example.com',
      tel: '123-45-67',
      number: 29,
      range: 10,
      url: 'http://example.com',
      search: 'blabla',
      color: '#6a4415',
      date: null,
      time: '12:30',
      datetime: null,
      month: null,
      week: null
    };
 
    $scope.pricegroups = [
      {value: 1, label: 'Festivals'},
      {value: 2, label: 'Online Sale'}
    ];
	
	$scope.correctlySelected = $scope.pricegroups[0];
	 

    /* There is probably a better way than iterating through the whole list */
    $scope.showProductName = function(id) {  
      if (id)
      {
        var p = findProduct(id); 
        if (p) 
        { 
          return p.name;
        }
        else 
        {
          return "";
        } 
      }
      else 
      {
        return "";
      }
    };

    $scope.productChanged = function(id, row) {    
    }
 
    // editable table
    $scope.items = [];
  	// total sum of totals
  	$rootScope.totalSum = 0;
	
    $scope.groups = [];

    function findProduct(id)
    {
      for (var i = 0; i < $scope.products.length; i++)
      {
        if ($scope.products[i].id == id)
        {
          return $scope.products[i];
        }
      }
      return null;
    }

    $scope.loadGroups = function() {
      return $scope.groups.length ? null : $http.get('api/groups').success(function(data) {
        $scope.groups = data;
      });
    };

    $scope.showGroup = function(user) {
      if(user.group && $scope.groups.length) {
        var selected = $filter('filter')($scope.groups, {id: user.group});
        return selected.length ? selected[0].text : 'Not set';
      } else {
        return user.groupName || 'Not set';
      }
    };

    $scope.showStatus = function(user) {
      var selected = [];
      if(user && user.status) {
        selected = $filter('filter')($scope.statuses, {value: user.status});
      }
      return selected.length ? selected[0].text : 'Not set';
    };

    $scope.checkName = function(data) {
      if (data == null || data == '' ) {
        return "This field can not be empty";
      }
    };

    $scope.saveItem = function(data, item) { 
    };


    $scope.updateRow = function(data, item) {  
      var p = findProduct(item.id);
      if (p) 
      {
          item.name = p.name;
          item.instock = p.instock;
          item.unitprice = p.unitprice;
          item.tax = (item.unitprice - item.discount) * item.quantity * 0.1;

          var t = (item.unitprice * item.quantity)*1 + item.tax  - item.discount;
          console.log(t); 
          $rootScope.items = $scope.items;
          $rootScope.totalSum = $rootScope.totalSum + t; 
          return;
      }
      else 
      {

      } 

    };


    // remove item
    $scope.removeItem = function(index) {
  		$rootScope.items = $scope.items;
  		$rootScope.totalSum =$rootScope.totalSum- $scope.items[index].total;
  		$scope.items.splice(index, 1);
    };


    // add user
    $scope.addItem = function() {
      $scope.inserted = {
        name: '',
        quantity: '1',
    		instock: null,
    		unitprice: null,
    		discount: '0',
    		tax:null,
        total: null
		
      };
      $scope.items.push($scope.inserted);
	  
    };

    $scope.hasPaypalReceiptView = function()
    {
      console.log("He has a Paypal receipt");
    }

    $scope.makePaypalNowView = function() 
    {
      console.log("He wants to pay with Paypal now.");
    }

	// currency
	$scope.currency = 'AUD';
	
	//orderNumber
	$scope.orderNumber = '0123456';


	
}]);



app.controller('DataTable', ['$scope', '$rootScope', 
  function($scope,$rootScope){


  /**
    Set view states
  */

    $scope.payViewState = 
    {
      cash : { show: false},
      paypal : { show : true, receipt:  false, pay : false}
    }; 


    function setCashPayView()
    {
      $scope.payViewState.cash.show = true;
      $scope.payViewState.paypal.show = false;
    }

    function setPaypalChooseView() 
    {
      $scope.payViewState.cash.show = false;
      $scope.payViewState.paypal.show = true;
    }

    function setPaypalPayView() 
    {
      $scope.payViewState.paypal.receipt = false;
      $scope.payViewState.paypal.pay = true;
    }

    function setPaypalReceiptView() 
    {
      $scope.payViewState.paypal.receipt = true;
      $scope.payViewState.paypal.pay = false;
    }


	  $scope.trackingnumber = "";
	  $scope.paypalReferenceNumber = "";
	  $scope.cashReferenceNumber = "";
	  
	  $rootScope.trackingNumber = "No Tracking Number";
	  $rootScope.referenceNumber = "No Reference Number";
	  
	  $scope.shippingMethods = [
		  {name:'Australia Post', value:'23'},
		  {name:'US Post', value:'32'}
	  ];
	  
	  $scope.shippingMethod = $scope.shippingMethods[0];
	  $rootScope.shippingMethodFee = $scope.shippingMethods[0].value;
	  $rootScope.shippingMethodName = $scope.shippingMethods[0].name;
	  
	  $scope.updateShippingMethod = function() {
	    $rootScope.shippingMethodFee = $scope.shippingMethod.value;
	    $rootScope.shippingMethodName = $scope.shippingMethod.name;
	  };
	  
	  $scope.paymentMethods = [
		  {name:'Paypal', value:'1'},
		  {name:'Cash on Hand', value:'2'}
	  ];
	  $scope.paymentMethod = $scope.paymentMethods[0];
	  
	  $scope.isPaypal = true;
	  $rootScope.pMethod = "Paypal";

	  $scope.updatePaymentMethod = function(obj) {
    		if(obj.paymentMethod.value == 1)
    		{
          setPaypalChooseView();
    			$rootScope.pMethod = "Paypal";
    			$rootScope.referenceNumber = $scope.paypalReferenceNumber;
    		}
    		else
    		{
          setCashPayView(); 
    			$rootScope.pMethod = "Cash on Hand";
    			$rootScope.referenceNumber = $scope.cashReferenceNumber;
    		}
    		
    		$rootScope.isPaypal = $scope.isPaypal;
    };
	  $scope.$watch('payViewState.paypal.receipt', function(value) {
       if (value) {
          $scope.payViewState.paypal.pay = false; 
       }
    });

    $scope.$watch('payViewState.paypal.pay', function(value) {
       if (value) {
          $scope.payViewState.paypal.receipt = false; 
       }
    });

    $scope.onPaypalOptionChecked = function(value)
    { 
    };

	  $scope.onTrackingNumberChange = function()
	  {
		  $rootScope.trackingNumber = $scope.trackingnumber;
	  };
	  
    $scope.onCashReferenceNumberChange = function(obj)
	  {
		  $rootScope.referenceNumber = obj.cashReferenceNumber;
		  $scope.cashReferenceNumber = obj.cashReferenceNumber;
	  };

	  $scope.onPaypalReferenceNumberChange = function(obj)
	  {
		  $rootScope.referenceNumber = obj.paypalReferenceNumber;
		  $scope.paypalReferenceNumber = obj.paypalReferenceNumber;
	  };

    $scope.openPaypal = function() 
    { 
      $scope.steps.step4Dis=false;
      $scope.steps.step4=true;   
      var w = window.open('http://localhost:8080/wheresmystock/mockPaypal.html','_blank');
      w.scope = $scope; 
    }
 
  }
  
]);
