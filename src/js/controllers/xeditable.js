
app.controller('XeditableCtrl', ['$scope', '$rootScope', '$filter', '$http', 'editableOptions', 'editableThemes', 
  function($scope,$rootScope, $filter, $http, editableOptions, editableThemes){
    editableThemes.bs3.inputClass = 'input-sm';
    editableThemes.bs3.buttonsClass = 'btn-sm';
    editableOptions.theme = 'bs3';

    $scope.html5 = {
      email: 'email@example.com',
      tel: '123-45-67',
      number: 29,
      range: 10,
      url: 'http://example.com',
      search: 'blabla',
      color: '#6a4415',
      date: null,
      time: '12:30',
      datetime: null,
      month: null,
      week: null
    };

    $scope.user = {
    	name: 'awesome',
    	desc: 'Awesome user \ndescription!',
      status: 2,
      agenda: 1,
      remember: false
    }; 

    $scope.pricegroups = [
      {value: 1, label: 'Festivals'},
      {value: 2, label: 'Online Sale'}
    ];
	
	$scope.correctlySelected = $scope.pricegroups[0];
	
    $scope.agenda = [
      {value: 1, text: 'male'},
      {value: 2, text: 'female'}
    ];

    $scope.showStatus = function() {
      var selected = $filter('filter')($scope.statuses, {value: $scope.user.status});
      return ($scope.user.status && selected.length) ? selected[0].text : 'Not set';
    };

    $scope.showAgenda = function() {
      var selected = $filter('filter')($scope.agenda, {value: $scope.user.agenda});
      return ($scope.user.agenda && selected.length) ? selected[0].text : 'Not set';
    };

    // editable table
    $scope.items = [];
  	// total sum of totals
  	$rootScope.totalSum = 0;
	
    $scope.groups = [];
    $scope.loadGroups = function() {
      return $scope.groups.length ? null : $http.get('api/groups').success(function(data) {
        $scope.groups = data;
      });
    };

    $scope.showGroup = function(user) {
      if(user.group && $scope.groups.length) {
        var selected = $filter('filter')($scope.groups, {id: user.group});
        return selected.length ? selected[0].text : 'Not set';
      } else {
        return user.groupName || 'Not set';
      }
    };

    $scope.showStatus = function(user) {
      var selected = [];
      if(user && user.status) {
        selected = $filter('filter')($scope.statuses, {value: user.status});
      }
      return selected.length ? selected[0].text : 'Not set';
    };

    $scope.checkName = function(data) {
      if (data == '' ) {
        return "This field can not be empty";
      }
    };

    $scope.saveItem = function(data, id) {
      //$scope.user not updated yet
	  var t = (data.unitprice * data.quantity)*1 + ((data.tax/100)*(data.unitprice * data.quantity))*1  - data.discount;
      angular.extend(data, {id: id});
	  $rootScope.items = $scope.items;
	  $rootScope.totalSum = $rootScope.totalSum + t;
      // return $http.post('api/saveUser', data);
    };

    // remove item
    $scope.removeItem = function(index) {
		$rootScope.items = $scope.items;
		$rootScope.totalSum =$rootScope.totalSum- $scope.items[index].total;
		$scope.items.splice(index, 1);
    };


    // add user
    $scope.addItem = function() {
      $scope.inserted = {
        name: '',
        quantity: '1',
    		instock: null,
    		unitprice: null,
    		discount: null,
    		tax:null,
        total: null
		
      };
      $scope.items.push($scope.inserted);
	  
    };

	// currency
	$scope.currency = 'AUD';
	
	//orderNumber
	$scope.orderNumber = '0123456';
	
}]);



app.controller('DataTable', ['$scope', '$rootScope', 
  function($scope,$rootScope){
	  $scope.trackingnumber = "";
	  $scope.paypalReferenceNumber = "";
	  $scope.cashReferenceNumber = "";
	  
	  $rootScope.trackingNumber = "No Tracking Number";
	  $rootScope.referenceNumber = "No Reference Number";
	  
	  $scope.shippingMethods = [
		  {name:'Australia Post', value:'23'},
		  {name:'US Post', value:'32'}
	  ];
	  
	  $scope.shippingMethod = $scope.shippingMethods[0];
	  $rootScope.shippingMethodFee = $scope.shippingMethods[0].value;
	  $rootScope.shippingMethodName = $scope.shippingMethods[0].name;
	  
	  $scope.updateShippingMethod = function() {
	    $rootScope.shippingMethodFee = $scope.shippingMethod.value;
	    $rootScope.shippingMethodName = $scope.shippingMethod.name;
	  };
	  
	  $scope.paymentMethods = [
		  {name:'Paypal', value:'1'},
		  {name:'Cash on Hand', value:'2'}
	  ];
	  $scope.paymentMethod = $scope.paymentMethods[0];
	  
	  $scope.isPaypal = true;
	  $rootScope.pMethod = "Paypal";
	  $scope.updatePaymentMethod = function(obj) {
		if(obj.paymentMethod.value == 1)
		{
			$scope.isPaypal = true;
			$rootScope.pMethod = "Paypal";
			$rootScope.referenceNumber = $scope.paypalReferenceNumber;
		}
		else
		{
			$scope.isPaypal = false;
			$rootScope.pMethod = "Cash on Hand";
			$rootScope.referenceNumber = $scope.cashReferenceNumber;
		}
		
		$rootScope.isPaypal = $scope.isPaypal;
	 };
	  
	  $scope.onTrackingNumberChange = function()
	  {
		  $rootScope.trackingNumber = $scope.trackingnumber;
	  };
	  $scope.onCashReferenceNumberChange = function(obj)
	  {
		  $rootScope.referenceNumber = obj.cashReferenceNumber;
		  $scope.cashReferenceNumber = obj.cashReferenceNumber;
		  
	  };
	  $scope.onPaypalReferenceNumberChange = function(obj)
	  {
		  $rootScope.referenceNumber = obj.paypalReferenceNumber;
		  $scope.paypalReferenceNumber = obj.paypalReferenceNumber;
	  };
	  
  }
  
]);
