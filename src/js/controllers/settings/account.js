'use string';


app.controller('PersonalCtrl', function($scope, $filter, $http) {

    $scope.myImage=''; 
    $scope.titleT = 'Personal profile';
    var handleFileSelect=function(evt) {
      var file=evt.currentTarget.files[0];
      var reader = new FileReader();
      reader.onload = function (evt) {
        $scope.$apply(function($scope){
          $scope.myImage=evt.target.result;
        });
      };
      reader.readAsDataURL(file);
    };
    angular.element(document.querySelector('#fileInput')).on('change',handleFileSelect);

});
 
app.controller('CompanyCtrl', function($scope, $filter, $http) {
    $scope.companyImage= ''; 
    $scope.titleT = 'Company Profile';
    var handleFileSelect=function(evt) {
      var file=evt.currentTarget.files[0];
      var reader = new FileReader();
      reader.onload = function (evt) {
        $scope.$apply(function($scope){
          $scope.companyImage=evt.target.result;
        });
      };
      reader.readAsDataURL(file);
    };
    angular.element(document.querySelector('#fileInput')).on('change',handleFileSelect);

});

app.controller('SecurityCtrl', function($scope, $filter, $http, editableOptions, editableThemes) { 
});

app.controller('SubscriptionCtrl', function($scope, $filter, $http, editableOptions, editableThemes) { 
});


app.controller('TeamCtrl', function($scope, $filter, $http, editableOptions, editableThemes) { 
});

