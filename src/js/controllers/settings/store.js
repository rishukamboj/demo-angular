'use strict';

app.controller('PriceGroupCtrl', function($scope, $filter, $http, editableOptions, editableThemes) {
  editableThemes.bs3.inputClass = 'input-sm';
  editableThemes.bs3.buttonsClass = 'btn-sm';
  editableOptions.theme = 'bs3';

  $scope.priceGroups = [
    {id: 1, name: 'Festival', discount: 5.00, type: 2},
    {id: 2, name: 'Summer', discount: 10.00, type: 1},
    {id: 3, name: 'Winter', discount: 4.50, type: 1}
  ]; 

  $scope.types = [
    {value: 1, text: 'Percentage'},
    {value: 2, text: 'Fixed'},
  ]; 

  $scope.showTypes = function(selectedType) {
    var selected = $filter('filter')($scope.types, {value: selectedType});
    return (selectedType && selected.length) ? selected[0].text : 'Not set';
  };

  $scope.checkName = function(data, id) {
    if (id === 2 && data !== 'awesome') {
      return "Username 2 should be `awesome`";
    }
  };

  $scope.savePriceGroups = function(data, id) {
    //$scope.user not updated yet
    angular.extend(data, {id: id});
    //return $http.post('/saveUser', data);
  };

  // remove user
  $scope.removePriceGroups = function(index) {
    $scope.priceGroups.splice(index, 1);
  };

  // add user
  $scope.addPriceGroups = function() {
    $scope.inserted = {
      id: $scope.priceGroups.length+1,
      name: '',
      discount: null,
      type: 'percentage' 
    };
    $scope.priceGroups.push($scope.inserted);
  };
});
app.controller('CurrencyCtrl', function($scope, $filter, $http, editableOptions, editableThemes) {
  editableThemes.bs3.inputClass = 'input-sm';
  editableThemes.bs3.buttonsClass = 'btn-sm';
  editableOptions.theme = 'bs3';

  $scope.currency = 1;
  $scope.currencies = [
    {value: 1, text: 'AUD'},
    {value: 2, text: 'USD'},
    {value: 3, text: 'RUPEE'},
  ];
  $scope.showCurrency = function(currency) {
    var selected = $filter('filter')($scope.currencies, {value: currency});
    return (currency && selected.length) ? selected[0].text : 'Not set';
  };
});

app.controller('RemindersCtrl', function($scope, $filter, $http, editableOptions, editableThemes) {
  editableThemes.bs3.inputClass = 'input-sm';
  editableThemes.bs3.buttonsClass = 'btn-sm';
  editableOptions.theme = 'bs3';

  $scope.isEmailing = 1;
  $scope.productAmount = 12;
  
});