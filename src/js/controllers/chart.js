'use strict';
app.service('MyService', function () {
            this.GetRecord = function () { return [
			{
			    "id": 1,
			    "label": "Shirt #1",
			    "data": [[gd(2015, 0, 1), 2,{"Online":2,"FFestival":0,"Locations":"Australia","Total":250.00}], [gd(2015, 1, 1), 4,{"Online":2,"FFestival":2,"Locations":"Australia","Total":350.00}], [gd(2015, 2, 1), 5,{"Online":1,"FFestival":4,"Locations":"Australia","Total":650.00}], [gd(2015, 3, 1), 2,{"Online":1,"FFestival":1,"Locations":"Australia","Total":250.00}], [gd(2015, 4, 1), 4,{"Online":2,"FFestival":2,"Locations":"Australia","Total":250.00}], [gd(2015, 5, 1), 3,{"Online":2,"FFestival":1,"Locations":"Australia","Total":1250.00}], [gd(2015, 6, 1), 5],
	[gd(2015, 7, 1), 6,{"Online":2,"FFestival":4,"Locations":"Australia","Total":900.00}], [gd(2015, 8, 8), 10,{"Online":8,"FFestival":2,"Locations":"Australia","Total":250.00}], [gd(2015, 9, 1), 3,{"Online":2,"FFestival":1,"Locations":"Australia","Total":250.00}], [gd(2015, 10, 1), 35,{"Online":20,"FFestival":15,"Locations":"Australia","Total":2250.00}], [gd(2015, 11, 1), 2,{"Online":0,"FFestival":2,"Locations":"Australia","Total":250.00}]],
			    "color": '#46bb00',
				
			}
			,
			{
			    "id": 2,
			    "label": "Shirt #2",
			    "data": [[gd(2015, 0, 1), 12,{"Online":2,"FFestival":10,"Locations":"Australia","Total":540.00}], [gd(2015, 1, 1), 9,{"Online":5,"FFestival":4,"Locations":"Australia","Total":960.00}], [gd(2015, 2, 1), 7,{"Online":2,"FFestival":0,"Locations":"Australia","Total":250.00}], [gd(2015, 3, 1), 9,{"Online":2,"FFestival":7,"Locations":"Australia","Total":960.00}], [gd(2015, 4, 1), 6,{"Online":5,"FFestival":1,"Locations":"Australia","Total":960.00}], [gd(2015, 5, 1), 8,{"Online":5,"FFestival":3,"Locations":"Australia","Total":960.00}], [gd(2015, 6, 1), 4,{"Online":0,"FFestival":4,"Locations":"Australia","Total":960.00}],
					[gd(2015, 7, 1), 9,{"Online":4,"FFestival":5,"Locations":"Australia","Total":1930.00}], [gd(2015, 8, 8), 5,{"Online":1,"FFestival":4,"Locations":"Australia","Total":830.00}], [gd(2015, 9, 1), 2,{"Online":1,"FFestival":1,"Locations":"Australia","Total":350.00}], [gd(2015, 10, 1), 40,{"Online":25,"FFestival":15,"Locations":"Australia","Total":1760.00}], [gd(2015, 11, 1), 12,{"Online":8,"FFestival":4,"Locations":"Australia","Total":960.00}]],
			    "color": '#fe3365'
			},
               			{
               		    "id": 3,
              			    "label": "Shirt #3",
               			    "data": [[gd(2015, 0, 1), 22,{"Online":12,"FFestival":10,"Locations":"Australia","Total":9460.00}], [gd(2015, 1, 1), 19,{"Online":15,"FFestival":4,"Locations":"Australia","Total":1060.00}], [gd(2015, 2, 1), 10,{"Online":6,"FFestival":4,"Locations":"Australia","Total":1230.00}], [gd(2015, 3, 1), 21,{"Online":14,"FFestival":7,"Locations":"Australia","Total":2840.00}], [gd(2015, 4, 1), 16,{"Online":12,"FFestival":4,"Locations":"Australia","Total":1760.00}], [gd(2015, 5, 1), 18,{"Online":12,"FFestival":6,"Locations":"Australia","Total":2530.00}], [gd(2015, 6, 1), 14,{"Online":11,"FFestival":3,"Locations":"Australia","Total":1352.00}],
               					[gd(2015, 7, 1), 26,{"Online":16,"FFestival":6,"Locations":"Australia","Total":3324.00}], [gd(2015, 8, 8), 28,{"Online":5,"FFestival":23,"Locations":"Australia","Total":3432.00}], [gd(2015, 9, 1), 12,{"Online":3,"FFestival":9,"Locations":"Australia","Total":1540.00}], [gd(2015, 10, 1), 36,{"Online":25,"FFestival":11,"Locations":"Australia","Total":4327.00}], [gd(2015, 11, 1), 22,{"Online":12,"FFestival":10,"Locations":"Australia","Total":2823.00}]],
               			    "color": '#c9f8ab'
              		}
            ];
			};
			
			  function gd(year, month, day) {
                    // alert(new Date(year, month, day).getTime());
                    return new Date(year, month, day).getTime();
                }
				
				this.InitGraph=function()
				{
				};
             });
			 
			 
  // Flot Chart controller 
 app.controller('FlotChartDemoCtrl', ['$scope','MyService', function($scope,MyService) {
 // $( "#datepicker,#datepicker1" ).datepicker();
	//$("#ui-datepicker-div").css("top","77px");

	$scope.TotalRecord=MyService.GetRecord();
	$scope.dayActive = 'active';
    $scope.weekActive = '';
    $scope.monthActive = '';
	
	$scope.SelectedData=[];
		
	$scope.XxSetting={
                    /*min: (new Date(2015, 1, 1)).getTime(),
                    max: (new Date(2015, 12, 2)).getTime(),*/
                    mode: "time",
                    tickSize: [1, "month"],
                    monthNames: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                    // tickLength: 0,
                    axisLabel: 'Month',
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 11,
                    axisLabelPadding: 5,
					 color: '#ccc' 


                };
				setTimeout(function(){  InitGraph($scope.TotalRecord,false);},1000);
	$scope.productId=0;
	
	$scope.SetGraph = function () {
               
				var _result=false;
				if($scope.SelectedData.length>0)
				{
                                  
									$scope.SelectedData.forEach(function(itm)
									{
									if(itm.id==$scope.productId)
									{
									_result=true;
									}
									});
				}
				if(!_result)
				{
                    $scope.TotalRecord.forEach(function (d) {
                        if (d.id == $scope.productId) {
                            $scope.SelectedData.push({
                                "id": d.id,
                                "label": d.label,
                                "data": d.data,
                                "color": d.color
                            });
                        }
                    });
				}
				
				$scope.productId=0;
				    $scope.SelectedData.length>0?InitGraph($scope.SelectedData,true):InitGraph($scope.TotalRecord,false);
            };

	$scope.SetDay=function()
	{
        $scope.dayActive = 'active'; 
        $scope.weekActive = ''; 
        $scope.monthActive = ''; 
    	$scope.XxSetting={
                /*min: (new Date(2015, 1, 1)).getTime(),
                max: (new Date(2015, 12, 2)).getTime(),*/
                mode: "time",
    			minTickSize: [1, "day"],
    			min: (new Date(2015, 1, 1)).getTime(),
    			max: (new Date(2015, 1, 7)).getTime(),
    			 color: '#ccc' ,
    			timeformat: "%a"


            };
	   $scope.SelectedData.length>0?InitGraph($scope.SelectedData,true):InitGraph($scope.TotalRecord,false);
	};

	$scope.SetWeek=function()
	{ 
        $scope.dayActive = ''; 
        $scope.weekActive = 'active'; 
        $scope.monthActive = ''; 
    	$scope.XxSetting={
                /*min: (new Date(2015, 1, 1)).getTime(),
                max: (new Date(2015, 12, 2)).getTime(),*/
                mode: "time",
    			minTickSize: [1, "day"],
    			min: (new Date(2015, 1, 1)).getTime(),
    			max: (new Date(2015, 1, 7)).getTime(),
    			 color: '#ccc' ,
    			timeformat: "%d/%m/%Y"


            };
       $scope.SelectedData.length>0?InitGraph($scope.SelectedData,true):InitGraph($scope.TotalRecord,false); 
	};

	$scope.SetMonth=function()
	{
        $scope.dayActive = ''; 
        $scope.weekActive = ''; 
        $scope.monthActive = 'active'; 
    	$scope.XxSetting={
                /*min: (new Date(2015, 1, 1)).getTime(),
                max: (new Date(2015, 12, 2)).getTime(),*/
                mode: "time",
                tickSize: [1, "month"],
                monthNames: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                // tickLength: 0,
                axisLabel: 'Month',
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 11,
                axisLabelPadding: 5,
    			 color: '#ccc' 
            };
	  $scope.SelectedData.length>0?InitGraph($scope.SelectedData,true):InitGraph($scope.TotalRecord,false); 
	};
			
	$scope.close = function (id) {
          $scope.SelectedData.splice(id, 1);
       
	  $scope.SelectedData.length>0?InitGraph($scope.SelectedData,true):InitGraph($scope.TotalRecord,false);

    };
	
    var InitGraph=  function(data,IsChart) {
            //var data1 = d;
            $.plot($("#filled_green"), data, {
                xaxis: $scope.XxSetting ,
                yaxis: {
                    axisLabel: 'Quantity',
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 11,
                    autoscaleMargin: 0.01,
                    axisLabelPadding: 5,
					 color: '#ccc' 
                    // tickLength: "full"
                },
                series: {
                    lines: {
                        show: IsChart,
                        fill: false,
                        fillColor: { colors: [{ opacity: 0.2 }, { opacity: 0.2}] },
                        lineWidth: 1.5
                    },
                    points: {
                        show: IsChart,
                        radius: 2.5,
                        fill: true,
                        fillColor: "#ffffff",
                        symbol: "circle",
                        lineWidth: 1.1
                    }
                },
                grid: { hoverable: true, clickable: true, borderWidth: 1, color: '#ccc' },
                legend: {
                    show: false
                }
            });
			 $("#filled_green").UseTooltip();
        }
		 var previousPoint = null, previousLabel = null;
		 $.fn.UseTooltip = function () {
            $(this).bind("plothover", function (event, pos, item) {
                if (item) {
                    if ((previousLabel != item.series.label) || (previousPoint != item.dataIndex)) {
					console.log(item);
                        previousPoint = item.dataIndex;
                        previousLabel = item.series.label;
                        $("#tooltip").remove();
						var _sellRecord=null;
						angular.forEach(item.series.data,function(itm)
						{
							if(itm[0]==item.datapoint[0] && itm[1]==item.datapoint[1])
							{
								_sellRecord=itm[2];
							}
						});
                       
                        var totalSold = item.datapoint[1];
 
                        var color = item.series.color;
                       
 
                        //console.log(item);
 
                        if (item.seriesIndex == 0) {
                            showTooltip(item.pageX,
                            item.pageY,
                            color,                                                                          																																	//{"Online":2,"FFestival":0,"Locations":"Australia","Total":250.00}
                            "<span style='border-bottom:2px solid #cccccc;width:100%;textAlign:center;'><strong>" + item.series.label + "</strong></span><br>Units Sold : <strong>" + totalSold + "</strong><br>Location : <strong>" + _sellRecord.Locations + "</strong><br>Online : <strong>" + _sellRecord.Online + "</strong><br>FFestival : <strong>" + _sellRecord.FFestival + "</strong><br>Total : <strong>AU $" + _sellRecord.Total + "</strong>");
                        } else {
                            showTooltip(item.pageX,
                            item.pageY,
                            color,
                            "<span style='border-bottom:2px solid #cccccc;textAlign:center;'><strong>" + item.series.label + "</strong></span><br>Units Sold : <strong>" + totalSold + "</strong><br>Location : <strong>" + _sellRecord.Locations + "</strong><br>Online : <strong>" + _sellRecord.Online + "</strong><br>FFestival : <strong>" + _sellRecord.FFestival + "</strong><br>Total : <strong>AU $" + _sellRecord.Total + "</strong>");
                        }
                    }
                } else {
                    $("#tooltip").remove();
                    previousPoint = null;
                }
            });
        };
 
        function showTooltip(x, y, color, contents) {
            $('<div id="tooltip">' + contents + '</div>').css({
                position: 'absolute',
                display: 'none',
                top: y-30 ,
                left: x-140,
                border: '2px solid ' + color,
                padding: '3px',
                'font-size': '11px',
                'border-radius': '5px',
                'background-color': '#fff',
                'font-family': 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
                opacity: 0.9
            }).appendTo("body").fadeIn(200);
        }
		
		
		
		
		
		
		//===========================================================================================================



    $scope.d = [ [1,6.5],[2,6.5],[3,7],[4,8],[5,7.5],[6,7],[7,6.8],[8,7],[9,7.2],[10,7],[11,6.8],[12,7] ];

    $scope.d0_1 = [ [0,7],[1,6.5],[2,12.5],[3,7],[4,9],[5,6],[6,11],[7,6.5],[8,8],[9,7] ];

    $scope.d0_2 = [ [0,4],[1,4.5],[2,7],[3,4.5],[4,3],[5,3.5],[6,6],[7,3],[8,4],[9,3] ];

    $scope.d1_1 = [ [10, 120], [20, 70], [30, 70], [40, 60] ];

    $scope.d1_2 = [ [10, 50],  [20, 60], [30, 90],  [40, 35] ];

    $scope.d1_3 = [ [10, 80],  [20, 40], [30, 30],  [40, 20] ];

    $scope.d2 = [];

    for (var i = 0; i < 20; ++i) {
      $scope.d2.push([i, Math.round( Math.sin(i)*100)/100] );
    }   

    $scope.d3 = [ 
      { label: "iPhone5S", data: 40 }, 
      { label: "iPad Mini", data: 10 },
      { label: "iPad Mini Retina", data: 20 },
      { label: "iPhone4S", data: 12 },
      { label: "iPad Air", data: 18 }
    ];

    $scope.refreshData = function(){
      $scope.d0_1 = $scope.d0_2;
    };

    $scope.getRandomData = function() {
      var data = [],
      totalPoints = 150;
      if (data.length > 0)
        data = data.slice(1);
      while (data.length < totalPoints) {
        var prev = data.length > 0 ? data[data.length - 1] : 50,
          y = prev + Math.random() * 10 - 5;
        if (y < 0) {
          y = 0;
        } else if (y > 100) {
          y = 100;
        }
        data.push(Math.round(y*100)/100);
      }
      // Zip the generated y values with the x values
      var res = [];
      for (var i = 0; i < data.length; ++i) {
        res.push([i, data[i]])
      }
      return res;
    }

    $scope.d4 = $scope.getRandomData();
  }]);