'use strict';

/**
 * Config for the router
 */
angular.module('app')
  .run(
    [          '$rootScope', '$state', '$stateParams',
      function ($rootScope,   $state,   $stateParams) {
          $rootScope.$state = $state;
          $rootScope.$stateParams = $stateParams;        
      }
    ]
  )
  .config(
    [          '$stateProvider', '$urlRouterProvider', 'JQ_CONFIG', 
      function ($stateProvider,   $urlRouterProvider, JQ_CONFIG) {
          
          $urlRouterProvider
              .otherwise('/app/orders');
          $stateProvider
              .state('app', {
                  abstract: true,
                  url: '/app',
                  templateUrl: 'tpl/app.html'
              }) 
              .state('app.orders', {
                  url: '/orders',
                  templateUrl: 'tpl/salesorders/app_orders.html',
                  
              }) 
              .state('intelligence', {
                  abstract: true,
                  url: '/intelligence', 
                  templateUrl: 'tpl/app.html'
              }) 
              .state('intelligence.products', {
                  url: '/products', 
                  templateUrl: 'tpl/intelligence/ui_intelligence_products.html',
                  resolve: {
                      deps: ['uiLoad',
                        function( uiLoad){
                          return uiLoad.load('js/controllers/intelligence/byproducts.js');
                      }]
                  }
              }) 
              .state('intelligence.totalsales', {
                  url: '/period', 
                  templateUrl: 'tpl/intelligence/ui_intelligence_totalsales.html',
                  resolve: {
                      deps: ['uiLoad',
                        function( uiLoad){
                          return uiLoad.load('js/controllers/intelligence/bytotalsales.js');
                      }]
                  }
              })
              .state('intelligence.bestsellers', {
                  url: '/bestsellers', 
                  templateUrl: 'tpl/intelligence/ui_intelligence_bestsellers.html',  
                  resolve: {
                      deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load('smart-table').then(
                              function(){
                                  return $ocLazyLoad.load('js/controllers/intelligence/bybestsellers.js');
                              }
                          );
                      }]
                  }
              })

              .state('settings', {
                  abstract: true,
                  url: '/settings', 
                  templateUrl: 'tpl/app.html',
                  resolve: {
                    deps: ['$ocLazyLoad',
                    function( $ocLazyLoad ){
                      return $ocLazyLoad.load('js/controllers/settings/store.js');
                    }]
                  }
              }) 
              .state('settings.store', {
                  url: '/store', 
                  templateUrl: 'tpl/settings/store.html',
                  resolve: {
                    deps: ['$ocLazyLoad',
                    function( $ocLazyLoad ){
                      return $ocLazyLoad.load('xeditable').then(
                        function(){
                          return $ocLazyLoad.load('js/controllers/xeditable.js');
                        }
                      );
                    }]
                  }
              })
              .state('settings.store.pricegroup', {
                  url: '/pricegroup', 
                  templateUrl: 'tpl/settings/store.pricegroup.html',                  
              })
              .state('settings.store.currency', {
                  url: '/currency', 
                  templateUrl: 'tpl/settings/store.currency.html',                  
              })
              .state('settings.store.reminders', {
                  url: '/reminders', 
                  templateUrl: 'tpl/settings/store.reminders.html',                  
              })

              .state('settings.account', {
                  url: '/account', 
                  templateUrl: 'tpl/settings/account.html',    
                  resolve: {
                    deps: ['$ocLazyLoad',
                    function( $ocLazyLoad ){
                      return $ocLazyLoad.load('js/controllers/settings/account.js');
                    }]
                  }              
              })
              .state('settings.account.personal', {
                  url: '/personal', 
                  templateUrl: 'tpl/settings/account.personal.html',                  
              })
              .state('settings.account.company', {
                  url: '/company', 
                  templateUrl: 'tpl/settings/account.company.html',                  
              })
              .state('settings.account.security', {
                  url: '/security', 
                  templateUrl: 'tpl/settings/account.security.html',                  
              })
              .state('settings.account.subscription', {
                  url: '/subscription', 
                  templateUrl: 'tpl/settings/account.subscription.html',                  
              })
              .state('settings.account.team', {
                  url: '/team', 
                  templateUrl: 'tpl/settings/account.team.html',                  
              })

              .state('settings.integration', {
                  url: '/integration', 
                  templateUrl: 'tpl/settings/integration.html',                  
              })

              .state('app.createorderform', {
                  url: '/createorderform',
                  views:{
                    '':{templateUrl: 'tpl/salesorders/create_order_form.html'  },
                    'createorder@app.createorderform':{
                      templateUrl: 'tpl/salesorders/new_order.html' ,
                      controller: 'SalesOrdersCtrl',
                      resolve: {
                        deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load('xeditable').then(
                            function(){
                              return $ocLazyLoad.load('js/controllers/salesorders/salesordersctrl.js');
                            }
                          );
                        }]
                      }
                    },
                    'readytoship@app.createorderform':{
                      templateUrl:'tpl/salesorders/ready_to_ship.html'
                    },
					'readytopay@app.createorderform':{
                      templateUrl:'tpl/salesorders/ready_to_pay.html'
                    },
					'readytoreview@app.createorderform':{
                      templateUrl:'tpl/salesorders/ready_to_review.html'
                    }
                  }
                  
              })


              .state('access', {
                  url: '/access',
                  template: '<div ui-view class="fade-in-right-big smooth"></div>'
              })
              .state('access.signin', {
                  url: '/signin',
                  templateUrl: 'tpl/access/page_signin.html',
                  resolve: {
                      deps: ['uiLoad',
                        function( uiLoad ){
                          return uiLoad.load( ['js/controllers/access/signin.js'] );
                      }]
                  }
              })
              .state('access.signup', {
                  url: '/signup',
                  templateUrl: 'tpl/access/page_signup.html',
                  resolve: {
                      deps: ['uiLoad',
                        function( uiLoad ){
                          return uiLoad.load( ['js/controllers/access/signup.js'] );
                      }]
                  }
              })
              .state('access.forgotpwd', {
                  url: '/forgotpwd',
                  templateUrl: 'tpl/access/page_forgotpwd.html'
              })

      }
    ]
  );
